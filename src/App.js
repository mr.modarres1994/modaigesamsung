import "./App.css";
import { useState } from "react";
import styled from "styled-components";
import { useWindowSize } from "react-use";
import Confetti from "react-confetti";
import HeaderImg from "./assets/img/header.jpg";
import YakanBakhLightEot from "./assets/fonts/YekanBakhFa-En03Light.eot";
import YakanBakhBoldWoff from "./assets/fonts/YekanBakhFa-En06Bold.woff";
import YakanBakhBlodEot from "./assets/fonts/YekanBakhFa-En06Bold.eot";
import YakanBakhLightWoff from "./assets/fonts/YekanBakhFa-En03Light.woff";
function App() {
  const { width, height } = useWindowSize();
  const [show, setShow] = useState(false);
  const clickHandler = () => {
    setShow(!show);
  };
  return (
    <Container>
      <Confetti
        numberOfPieces={`${show ? 500 : 0}`}
        width={width}
        height={height}
        tweenDuration={1000}
      />
      <Header>
        <img src={HeaderImg} alt="" />{" "}
      </Header>
      <Body>
        <Cards style={{ marginTop: "10px" }}>
          <Input />

          <Table>
            <thead>
              <th>شماره قرعه کشی</th>
              <th> شماره برندگان</th>
            </thead>

            <tr>
              <td>154876</td>
              <td>0917***8212</td>
            </tr>
            <tr>
              <td>154876</td>
              <td>0917***8212</td>
            </tr>
            <tr>
              <td>154876</td>
              <td>0917***8212</td>
            </tr>
          </Table>
        </Cards>
        <Cards>
          <Button onClick={clickHandler}>برنده</Button>
        </Cards>
      </Body>
    </Container>
  );
}
const Container = styled.div`
  @font-face {
    font-family: YakanBakh;
    src: url(${YakanBakhLightEot}) format("eot"),
      url(${YakanBakhBoldWoff}) format("woff"),
      url(${YakanBakhBlodEot}) format("eot"),
      url(${YakanBakhLightWoff}) format("woff");
  }
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  height: 100vh;
  background: #d43550;
`;
const Header = styled.header`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;

  top: 0px;
  left: 0px;
  right: 0px;

  img {
    width: 100%;
    top: 0px;
    bottom: 0px;
  }
`;
const Body = styled.body`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;
const Cards = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: column;
`;

const Button = styled.button`
  width: 250px;
  height: 50px;
  border-radius: 4px;
  border: none;
  font-size: 22px;
  background: #ffffff;

  color: #5a5a5a;
  &:hover {
    opacity: 0.8;
  }
`;

const Table = styled.table`
  font-family: YekanBakh;

  width: 40%;
  height: 150px;
  border: 1px solid #f7677f;
  border-radius: 10px;
  background: #f7677f;
  thead {
    background-color: #ffffff;
    th {
      font-weight: 800;
      font-size: 30px;
    }
  }
  tr {
    td {
      text-align: center;
      /* font-weight: ; */
      /* border: 1px solid blue; */
      /* box-sizing: border-box; */
    }
  }
`;
const Input = styled.input`
  border: 1px solid #feaebb;
  border-radius: 10px;
  width: 40%;
  letter-spacing: 20px;
  font-size: 60px;
  text-align: center;
  height: 70px;
  background: #feaebb;
  color: rgb(249, 249, 249);
`;
export default App;
